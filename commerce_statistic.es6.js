/**
 * @file
 * Statistics functionality.
 */

(function($, Drupal, drupalSettings) {
  $(document).ready(() => {
    $.ajax({
      type: 'POST',
      cache: false,
      url: drupalSettings.commerce_statistic.url,
      data: drupalSettings.commerce_statistic.data,
    });
  });
})(jQuery, Drupal, drupalSettings);
