<?php

/**
 * @file
 * Provide views data for commerce_statistic.module.
 */

/**
 * Implements hook_views_data().
 */
function commerce_statistic_views_data() {
  $data['product_counter']['table']['group'] = t('Commerce statistic');

  $data['product_counter']['table']['join'] = [
    'commerce_product_field_data' => [
      'left_field' => 'product_id',
      'field' => 'product_id',
    ],
  ];

  $data['product_counter']['totalcount'] = [
    'title' => t('Total views'),
    'help' => t('The total number of times the product has been viewed.'),
    'field' => [
      'id' => 'commerce_statistic_numeric',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['product_counter']['daycount'] = [
    'title' => t('Views today'),
    'help' => t('The total number of times the product has been viewed today.'),
    'field' => [
      'id' => 'commerce_statistic_numeric',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['product_counter']['timestamp'] = [
    'title' => t('Most recent view'),
    'help' => t('The most recent time the product has been viewed.'),
    'field' => [
      'id' => 'product_counter_timestamp',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'id' => 'date',
    ],
    'argument' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  return $data;
}
