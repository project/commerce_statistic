<?php

/**
 * @file
 * Builds replacement tokens for product Visitor commerce_statistic.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function commerce_statistic_token_info() {
  $product['total-count'] = [
    'name' => t("Number of views"),
    'description' => t("The number of visitors who have read the product."),
  ];
  $product['day-count'] = [
    'name' => t("Views today"),
    'description' => t("The number of visitors who have read the product today."),
  ];
  $product['last-view'] = [
    'name' => t("Last view"),
    'description' => t("The date on which a visitor last read the product."),
    'type' => 'date',
  ];

  return [
    'tokens' => ['product' => $product],
  ];
}

/**
 * Implements hook_tokens().
 */
function commerce_statistic_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $token_service = \Drupal::token();

  $replacements = [];

  if ($type == 'commerce_product' & !empty($data['product'])) {
    $product = $data['product'];
    /** @var \Drupal\commerce_statistic\CommerceStatisticStorageInterface $stats_storage */
    $stats_storage = \Drupal::service('commerce_statistic.storage.product');
    foreach ($tokens as $name => $original) {
      if ($name == 'total-count') {
        $replacements[$original] = $stats_storage->fetchView($product->id())->getTotalCount();
      }
      elseif ($name == 'day-count') {
        $replacements[$original] = $stats_storage->fetchView($product->id())->getDayCount();
      }
      elseif ($name == 'last-view') {
        $replacements[$original] = \Drupal::service('date.formatter')->format($stats_storage->fetchView($product->id())->getTimestamp());
      }
    }

    if ($created_tokens = $token_service->findWithPrefix($tokens, 'last-view')) {
      $replacements += $token_service->generate('date', $created_tokens, ['date' => $stats_storage->fetchView($product->id())->getTimestamp()], $options, $bubbleable_metadata);
    }
  }

  return $replacements;
}
