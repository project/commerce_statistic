<?php

/**
 * @file
 * Handles counts of product views via AJAX with minimal bootstrap.
 */

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;

chdir('../../..');

$autoloader = require_once 'autoload.php';

$kernel = DrupalKernel::createFromRequest(Request::createFromGlobals(), $autoloader, 'prod');
$kernel->boot();
$container = $kernel->getContainer();

$views = $container
  ->get('config.factory')
  ->get('commerce_statistic.settings')
  ->get('count_content_views');

if ($views) {
  $product_id = filter_input(INPUT_POST, 'product_id', FILTER_VALIDATE_INT);
  if ($product_id) {
    $container->get('request_stack')->push(Request::createFromGlobals());
    $container->get('commerce_statistic.storage.product')->recordView($product_id);
  }
}
