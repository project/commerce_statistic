<?php

namespace Drupal\commerce_statistic\Plugin\views\field;

use Drupal\views\Plugin\views\field\Date;
use Drupal\Core\Session\AccountInterface;

/**
 * Field handler to display the most recent time the product has been viewed.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("product_counter_timestamp")
 */
class ProductCounterTimestamp extends Date {

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    return $account->hasPermission('view product access counter');
  }

}
