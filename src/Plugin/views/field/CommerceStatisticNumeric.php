<?php

namespace Drupal\commerce_statistic\Plugin\views\field;

use Drupal\views\Plugin\views\field\NumericField;
use Drupal\Core\Session\AccountInterface;

/**
 * Field handler to display numeric values from commerce_statistic module.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("commerce_statistic_numeric")
 */
class CommerceStatisticNumeric extends NumericField {

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    return $account->hasPermission('view product access counter');
  }

}
