<?php

namespace Drupal\commerce_statistic\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\commerce_statistic\CommerceStatisticStorageInterface;

/**
 * Provides a 'Popular product' block.
 *
 * @Block(
 *   id = "commerce_statistic_popular_block",
 *   admin_label = @Translation("Popular product")
 * )
 */
class CommerceStatisticPopularBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The storage for commerce_statistic.
   *
   * @var \Drupal\commerce_statistic\CommerceStatisticStorageInterface
   */
  protected $commerceStatisticStorage;

  /**
   * The renderer interface.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs an StatisticsPopularBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\commerce_statistic\CommerceStatisticStorageInterface $commerce_statistic_storage
   *   The storage for commerce_statistic.
   * @param Drupal\Core\Render\RendererInterface $renderer
   *   The renderer interface.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityRepositoryInterface $entity_repository, CommerceStatisticStorageInterface $commerce_statistic_storage, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository = $entity_repository;
    $this->commerceStatisticStorage = $commerce_statistic_storage;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity.repository'),
      $container->get('commerce_statistic.storage.product'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'top_day_num' => 0,
      'top_all_num' => 0,
      'top_last_num' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    // Popular product block settings.
    $numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 30, 40];
    $numbers = ['0' => $this->t('Disabled')] + array_combine($numbers, $numbers);
    $form['commerce_statistic_block_top_day_num'] = [
      '#type' => 'select',
      '#title' => $this->t("Number of day's top views to display"),
      '#default_value' => $this->configuration['top_day_num'],
      '#options' => $numbers,
      '#description' => $this->t('How many content items to display in "day" list.'),
    ];
    $form['commerce_statistic_block_top_all_num'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of all time views to display'),
      '#default_value' => $this->configuration['top_all_num'],
      '#options' => $numbers,
      '#description' => $this->t('How many content items to display in "all time" list.'),
    ];
    $form['commerce_statistic_block_top_last_num'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of most recent views to display'),
      '#default_value' => $this->configuration['top_last_num'],
      '#options' => $numbers,
      '#description' => $this->t('How many content items to display in "recently viewed" list.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['top_day_num'] = $form_state->getValue('commerce_statistic_block_top_day_num');
    $this->configuration['top_all_num'] = $form_state->getValue('commerce_statistic_block_top_all_num');
    $this->configuration['top_last_num'] = $form_state->getValue('commerce_statistic_block_top_last_num');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $content = [];

    if ($this->configuration['top_day_num'] > 0) {
      $product_ids = $this->commerceStatisticStorage->fetchAll('daycount', $this->configuration['top_day_num']);
      if ($product_ids) {
        $content['top_day'] = $this->productTitleList($product_ids, $this->t("Today's:"));
        $content['top_day']['#suffix'] = '<br />';
      }
    }

    if ($this->configuration['top_all_num'] > 0) {
      $product_ids = $this->commerceStatisticStorage->fetchAll('totalcount', $this->configuration['top_all_num']);
      if ($product_ids) {
        $content['top_all'] = $this->productTitleList($product_ids, $this->t('All time:'));
        $content['top_all']['#suffix'] = '<br />';
      }
    }

    if ($this->configuration['top_last_num'] > 0) {
      $product_ids = $this->commerceStatisticStorage->fetchAll('timestamp', $this->configuration['top_last_num']);
      $content['top_last'] = $this->productTitleList($product_ids, $this->t('Last viewed:'));
      $content['top_last']['#suffix'] = '<br />';
    }

    return $content;
  }

  /**
   * Generates the ordered array of product links for build().
   *
   * @param int[] $product_ids
   *   An ordered array of product ids.
   * @param string $title
   *   The title for the list.
   *
   * @return array
   *   A render array for the list.
   */
  protected function productTitleList(array $product_ids, $title) {
    $products = $this->entityTypeManager->getStorage('commerce_product')->loadMultiple($product_ids);

    $items = [];
    foreach ($product_ids as $product_id) {
      $product = $this->entityRepository->getTranslationFromContext($products[$product_id]);
      $item = $product->toLink()->toRenderable();
      $this->renderer->addCacheableDependency($item, $product);
      $items[] = $item;
    }

    return [
      '#theme' => 'item_list__node',
      '#items' => $items,
      '#title' => $title,
      '#cache' => [
        'tags' => $this->entityTypeManager->getDefinition('commerce_product')->getListCacheTags(),
      ],
    ];
  }

}
