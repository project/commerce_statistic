<?php

namespace Drupal\commerce_statistic;

/**
 * Value object for passing statistic results.
 */
class CommerceStatisticViewsResult {

  /**
   * Store viewed of total.
   *
   * @var int
   *   Store viewed of total.
   */
  protected $totalCount;

  /**
   * Store viewed of a day.
   *
   * @var int
   *   Store viewed of a day.
   */
  protected $dayCount;

  /**
   * Store timestamp.
   *
   * @var int
   *   Store timestamp.
   */
  protected $timestamp;

  /**
   * Construct a view result.
   */
  public function __construct($total_count, $day_count, $timestamp) {
    $this->totalCount = (int) $total_count;
    $this->dayCount = (int) $day_count;
    $this->timestamp = (int) $timestamp;
  }

  /**
   * Total number of times the entity has been viewed.
   *
   * @return int
   *   Return number of times entity has been viewed.
   */
  public function getTotalCount() {
    return $this->totalCount;
  }

  /**
   * Total number of times the entity has been viewed "today".
   *
   * @return int
   *   Return number of times viewed today.
   */
  public function getDayCount() {
    return $this->dayCount;
  }

  /**
   * Timestamp of when the entity was last viewed.
   *
   * @return int
   *   Return timestamp.
   */
  public function getTimestamp() {
    return $this->timestamp;
  }

}
