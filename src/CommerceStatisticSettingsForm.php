<?php

namespace Drupal\commerce_statistic;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure commerce_statistic settings for this site.
 *
 * @internal
 */
class CommerceStatisticSettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The block manager.
   *
   * @var Drupal\Core\Block\BlockManager
   */
  protected $blockManager;

  /**
   * Constructs a \Drupal\commerce_statistic\StatisticsSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, BlockManagerInterface $block_manager) {
    parent::__construct($config_factory);

    $this->moduleHandler = $module_handler;
    $this->blockManager = $block_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('plugin.manager.block')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_statistic_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_statistic.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_statistic.settings');

    // Content counter settings.
    $form['content'] = [
      '#type' => 'details',
      '#title' => $this->t('Content viewing counter settings'),
      '#open' => TRUE,
    ];
    $form['content']['commerce_statistic_count_content_views'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Count content views'),
      '#default_value' => $config->get('count_content_views'),
      '#description' => $this->t('Increment a counter each time content is viewed.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('commerce_statistic.settings')
      ->set('count_content_views', $form_state->getValue('commerce_statistic_count_content_views'))
      ->save();

    // The popular commerce_statistic block is dependent on these settings,
    // so clear the block plugin definitions cache.
    if ($this->moduleHandler->moduleExists('block')) {
      $this->blockManager->clearCachedDefinitions();
    }

    parent::submitForm($form, $form_state);
  }

}
