<?php

namespace Drupal\commerce_statistic;

use Drupal\Core\Database\Connection;
use Drupal\Core\State\StateInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the default database storage backend for commerce_statistic.
 */
class ProductStatisticDatabaseStorage implements CommerceStatisticStorageInterface {

  /**
   * The database connection used.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs the commerce_statistic storage.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection for the product view storage.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The RequestStack.
   */
  public function __construct(Connection $connection, StateInterface $state, RequestStack $request_stack) {
    $this->connection = $connection;
    $this->state = $state;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public function recordView($id) {
    return (bool) $this->connection
      ->merge('product_counter')
      ->key('product_id', $id)
      ->fields([
        'daycount' => 1,
        'totalcount' => 1,
        'timestamp' => $this->getRequestTime(),
      ])
      ->expression('daycount', 'daycount + 1')
      ->expression('totalcount', 'totalcount + 1')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function fetchViews($ids) {
    $views = $this->connection
      ->select('product_counter', 'pc')
      ->fields('pc', ['totalcount', 'daycount', 'timestamp'])
      ->condition('product_id', $ids, 'IN')
      ->execute()
      ->fetchAll();
    foreach ($views as $id => $view) {
      $views[$id] = new CommerceStatisticViewsResult($view->totalcount, $view->daycount, $view->timestamp);
    }
    return $views;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchView($id) {
    $views = $this->fetchViews([$id]);
    return reset($views);
  }

  /**
   * {@inheritdoc}
   */
  public function fetchAll($order = 'totalcount', $limit = 5) {
    assert(in_array($order, [
      'totalcount',
      'daycount',
      'timestamp',
    ]), "Invalid order argument.");

    return $this->connection
      ->select('product_counter', 'pc')
      ->fields('pc', ['product_id'])
      ->orderBy($order, 'DESC')
      ->range(0, $limit)
      ->execute()
      ->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteViews($id) {
    return (bool) $this->connection
      ->delete('product_counter')
      ->condition('product_id', $id)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function resetDayCount() {
    $commerce_statistic_timestamp = $this->state->get('commerce_statistic.day_timestamp') ?: 0;
    if (($this->getRequestTime() - $commerce_statistic_timestamp) >= 86400) {
      $this->state->set('commerce_statistic.day_timestamp', $this->getRequestTime());
      $this->connection->update('product_counter')
        ->fields(['daycount' => 0])
        ->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function maxTotalCount() {
    $query = $this->connection->select('product_counter', 'pc');
    $query->addExpression('MAX(totalcount)');
    $max_total_count = (int) $query->execute()->fetchField();
    return $max_total_count;
  }

  /**
   * Get current request time.
   *
   * @return int
   *   Unix timestamp for current server request time.
   */
  protected function getRequestTime() {
    return $this->requestStack->getCurrentRequest()->server->get('REQUEST_TIME');
  }

}
