# Commerce Statistic

This module is a modification base on the core's statistic module.
It is specifically aim to collect view counts of the commerce_product entity.

## Installation

Install with ```composer require drupal/commerce_statistic```.

Please add the following line to your _.htaccess_ file in the site root folder.
This allows commerce_statistic.js to post it's data to the database table.

```
  # Allow access to Statistics module's custom front controller.
  # Copy and adapt this rule to directly execute PHP files in contributed or
  # custom modules or to run another PHP application in the same directory.
  RewriteCond %{REQUEST_URI} !/core/modules/statistics/statistics.php$

  // Add this line
  RewriteCond %{REQUEST_URI} !/modules/contrib/commerce_statistic/commerce_statistic.php$
```
